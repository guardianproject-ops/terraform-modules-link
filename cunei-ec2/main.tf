module "label" {
  source  = "git::https://github.com/cloudposse/terraform-null-label.git?ref=tags/0.19.2"
  context = module.this.context
  name    = "cunei"
  tags    = merge(module.this.tags, { Application = "cunei" })
}

module "instance" {
  source                        = "../ec2-instance"
  is_prod_like                  = var.is_prod_like
  ssm_prefix                    = var.ssm_prefix
  ssm_prefix_policy_arn         = var.ssm_prefix_policy_arn
  kms_key_arn                   = var.kms_key_arn
  secretsmanager_ssh_key_id     = var.secretsmanager_ssh_key_id
  vpc_id                        = var.vpc_id
  vpc_cidr_block                = var.vpc_cidr_block
  availability_zone_1           = var.availability_zone_1
  subnet_id                     = var.subnet_id
  disk_allocation_gb            = var.disk_allocation_gb
  instance_type                 = var.instance_type
  enable_ebs_volume             = true
  ebs_volume_disk_allocation_gb = var.ebs_volume_disk_allocation_gb
  ingress = {
    "cunei-backend-metrics" = {
      from_port = 9395,
      to_port   = 9395,
      protocol  = "tcp"
    },
    "cunei-worker-metrics" = {
      from_port = 9396,
      to_port   = 9396,
      protocol  = "tcp"
    },
  }
  context = module.label.context
}
