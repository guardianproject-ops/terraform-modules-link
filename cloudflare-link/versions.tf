terraform {
  required_providers {
    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = ">= 3.16.0"
    }
    aws = {
      source  = "hashicorp/aws"
      version = ">= 2.0"
    }
  }
}
