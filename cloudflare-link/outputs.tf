output "applications" {
  value = cloudflare_access_application.default
}

output "argo_tunnel_id" {
  value = cloudflare_argo_tunnel.default.id
}

output "argo_tunnel_name" {
  value = cloudflare_argo_tunnel.default.name
}

# output "canary_service_token_id" {
#   value = cloudflare_access_service_token.canary.client_id
# }
#
# output "canary_service_token_secret" {
#   value = cloudflare_access_service_token.canary.client_secret
# }

# output "leafcutter_data_service_token_id" {
#   value = cloudflare_access_service_token.leafcutter_data.client_id
# }
#
# output "leafcutter_data_service_token_secret" {
#   value = cloudflare_access_service_token.leafcutter_data.client_secret
# }
