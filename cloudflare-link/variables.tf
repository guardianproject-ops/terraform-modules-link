variable "cloudflare_edit_token" {
  type = string
}

variable "cloudflare_zone_id" {
  type = string
}

variable "cloudflare_account_id" {
  type = string
}

variable "access_applications" {
  type = map(object({
    domain = string
    name   = string
    roles  = list(string)
  }))
  description = "a map where the key is the argo tunnel id, and the value is the tunnel information"
}

variable "access_overrides" {
  type = map(object({
    domain = string
    name   = string
  }))
  description = "a map where the key is the argo tunnel id, and the value is the tunnel information"
}

variable "cloudflare_client_admin_group_id" {
  type = string
}

variable "cloudflare_client_agent_group_id" {
  type = string
}

variable "cloudflare_tunnel_secret" {
  type        = string
  description = "the shared secret used to authenticate to an argo tunnel. must be 32 random bytes base64 encoded"
}

variable "cloudflare_tunnel_name_override" {
  type        = string
  default     = ""
  description = "legacy value. do not use in new deployments"
}
