provider "cloudflare" {
  api_token = var.cloudflare_edit_token
}

data "aws_ssm_parameter" "cloudflare_super_admins_group_ids" {
  name     = "/shared/cdr.link/cloudflare_super_admins_group_ids"
  provider = aws.operations
}

resource "cloudflare_argo_tunnel" "default" {
  account_id = var.cloudflare_account_id
  name       = length(var.cloudflare_tunnel_name_override) > 0 ? var.cloudflare_tunnel_name_override : module.this.id
  secret     = var.cloudflare_tunnel_secret
}

resource "cloudflare_record" "tunnel_records" {
  for_each = var.access_applications
  zone_id  = var.cloudflare_zone_id
  name     = each.value.domain
  value    = "${cloudflare_argo_tunnel.default.id}.cfargotunnel.com"
  proxied  = true
  type     = "CNAME"
}

data "cloudflare_access_identity_provider" "google" {
  name = "Google"
  account_id = var.cloudflare_account_id
}

resource "cloudflare_access_application" "default" {
  for_each                  = var.access_applications
  zone_id                   = var.cloudflare_zone_id
  name                      = each.value.name
  domain                    = each.value.domain
  session_duration          = "12h"
  auto_redirect_to_identity = true
  allowed_idps = [data.cloudflare_access_identity_provider.google.id]
}

resource "cloudflare_access_policy" "allow_super_admins" {
  for_each       = cloudflare_access_application.default
  zone_id        = var.cloudflare_zone_id
  application_id = each.value.id
  name           = "Super Admins"
  precedence     = 1
  decision       = "allow"
  include {
    group = jsondecode(data.aws_ssm_parameter.cloudflare_super_admins_group_ids.value)
  }
}

resource "cloudflare_access_policy" "default_deny" {
  for_each       = cloudflare_access_application.default
  zone_id        = var.cloudflare_zone_id
  application_id = each.value.id
  name           = "Deny Public"
  precedence     = 100
  decision       = "deny"
  include {
    everyone = true
  }
}

resource "cloudflare_access_policy" "allow_admins" {
  for_each = {
    for k, d in var.access_applications :
    k => d
    if contains(d.roles, "admin")
  }

  zone_id        = var.cloudflare_zone_id
  application_id = cloudflare_access_application.default[each.key].id
  name           = "Client Admins"
  decision       = "allow"
  precedence     = 3
  include {
    group = [
      var.cloudflare_client_admin_group_id
    ]
  }
}

resource "cloudflare_access_policy" "allow_agents" {
  for_each = {
    for k, d in var.access_applications :
    k => d
    if contains(d.roles, "agent")
  }

  zone_id        = var.cloudflare_zone_id
  application_id = cloudflare_access_application.default[each.key].id
  name           = "Agents"
  decision       = "allow"
  precedence     = 2
  include {
    group = [
      var.cloudflare_client_agent_group_id
    ]
  }
}

resource "cloudflare_access_service_token" "sr2_monitoring" {
  account_id = var.cloudflare_account_id
  name       = "${module.this.id} :: SR2 Monitoring"
}

resource "cloudflare_access_policy" "allow_sr2_monitoring" {
  for_each = {
    for k, d in var.access_applications :
    k => d
    if contains(d.roles, "sr2_monitoring")
  }
  zone_id        = var.cloudflare_zone_id
  application_id = cloudflare_access_application.default[each.key].id
  name           = "SR2 Monitoring"
  decision       = "non_identity"
  precedence     = 7
  include {
    service_token = [
      cloudflare_access_service_token.sr2_monitoring.id
    ]
  }
}

resource "cloudflare_access_service_token" "canary" {
  account_id = var.cloudflare_account_id
  name       = "${module.this.id} :: AWS Canary"
}


resource "cloudflare_access_policy" "allow_canary" {
  for_each = {
    for k, d in var.access_applications :
    k => d
    if contains(d.roles, "canary")
  }

  zone_id        = var.cloudflare_zone_id
  application_id = cloudflare_access_application.default[each.key].id
  name           = "AWS Canary"
  decision       = "non_identity"
  precedence     = 4
  include {
    service_token = [
      cloudflare_access_service_token.canary.id
    ]
  }
}

resource "cloudflare_access_service_token" "leafcutter_data" {
  account_id = var.cloudflare_account_id
  name       = "${module.this.id} :: Leafcutter Data"
}

resource "cloudflare_access_policy" "allow_leafcutter_data" {
  for_each = {
    for k, d in var.access_applications :
    k => d
    if contains(d.roles, "leafcutter_data")
  }

  zone_id        = var.cloudflare_zone_id
  application_id = cloudflare_access_application.default[each.key].id
  name           = "Leafcutter Data"
  decision       = "non_identity"
  precedence     = 6
  include {
    service_token = [
      cloudflare_access_service_token.leafcutter_data.id
    ]
  }
}

resource "cloudflare_access_policy" "allow_public" {
  for_each = {
    for k, d in var.access_applications :
    k => d
    if contains(d.roles, "public")
  }
  zone_id        = var.cloudflare_zone_id
  application_id = cloudflare_access_application.default[each.key].id
  name           = "Public Access"
  decision       = "non_identity"
  precedence     = 5
  include {
    everyone = true
  }
}

resource "cloudflare_access_application" "override" {
  for_each                  = var.access_overrides
  zone_id                   = var.cloudflare_zone_id
  name                      = each.value.name
  domain                    = each.value.domain
  session_duration          = "12h"
  auto_redirect_to_identity = false
}

resource "cloudflare_access_policy" "allow_override" {
  for_each       = var.access_overrides
  zone_id        = var.cloudflare_zone_id
  application_id = cloudflare_access_application.override[each.key].id
  name           = "Override"
  decision       = "bypass"
  precedence     = 5
  include {
    everyone = true
  }
}
