variable "vpc_id" {
  description = "(Required) the vpc id that the eks cluster will be placed in"
  type = string
}

variable "private_subnet_ids" {
  description = "(Required) A list of private subnet ids to place fargate workers in"
  type = list(string)
}

variable "public_access" {
  description = "(Required) Indicates whether or not the Amazon EKS public API server endpoint is enabled."
  type = bool
}

variable "private_access" {
  description = "(Required) Indicates whether or not the Amazon EKS private API server endpoint is enabled."
  type = bool
}

variable "kms_key_arn" {
  description = "(Required) The KMS Key ARN used to encrypt eks secrets"
  type = string
}

variable "app_selectors" {
  description = "(Optional) Namespace selectors for the cluster."
  type = list(object({
    namespace = string
  }))
  default = []
}

variable "cluster_name" {
  description = "(Optional) The name of the cluster. If not provided, one is created from the label context. This value should be coordinated with the  'kubernetes.io/cluster/CLUSTER_NAME' tag on subnets for ELB placement."
  default = ""
  type = string
}

variable "cluster_version" {
  description = "(Required) The desired Kubernetes version for your EKS cluster."
  type = string
}
