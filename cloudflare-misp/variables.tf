variable "cloudflare_edit_token" {
  type = string
}

variable "cloudflare_zone_id" {
  type = string
}

variable "cloudflare_account_id" {
  type = string
}

variable "tunnels" {
  type = map(object({
    domain = string
    secret = string
    name   = string
    roles  = list(string)
  }))
  description = "a map where the key is the argo tunnel id, and the value is the tunnel information"
}

variable "cloudflare_client_user_group_id" {
  type = string
}

variable "cloudflare_access_service_token_canary_id" {
  type = string
}
