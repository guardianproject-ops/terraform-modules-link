terraform {
  required_providers {
    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = ">= 2.18.0"
    }
    aws = {
      source  = "hashicorp/aws"
      version = ">= 2.0"
    }
  }
}
