output "applications" {
  value = cloudflare_access_application.default
}

output "argo_tunnels" {
  value = cloudflare_argo_tunnel.default
}

#output "group_gp_admins" {
#  value = cloudflare_access_group.gp_admins.id
#}
#
#output "group_cdr_admins" {
#  value = cloudflare_access_group.cdr_admins.id
#}
