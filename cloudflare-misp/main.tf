provider "cloudflare" {
  api_token = var.cloudflare_edit_token
}

data "aws_ssm_parameter" "cloudflare_super_admins_group_ids" {
  name     = "/shared/cdr.link/cloudflare_super_admins_group_ids"
  provider = aws.operations
}

resource "cloudflare_argo_tunnel" "default" {
  for_each   = var.tunnels
  account_id = var.cloudflare_account_id
  name       = replace(each.value.domain, ".", "-")
  secret     = each.value.secret
}

resource "cloudflare_record" "tunnel_records" {
  for_each = var.tunnels
  zone_id  = var.cloudflare_zone_id
  name     = each.value.domain
  value    = "${cloudflare_argo_tunnel.default[each.key].id}.cfargotunnel.com"
  proxied  = true
  type     = "CNAME"
}

resource "cloudflare_access_application" "default" {
  for_each                  = var.tunnels
  zone_id                   = var.cloudflare_zone_id
  name                      = each.value.name
  domain                    = each.value.domain
  session_duration          = "12h"
  auto_redirect_to_identity = true
}

resource "cloudflare_access_policy" "allow_super_admins" {
  for_each       = cloudflare_access_application.default
  zone_id        = var.cloudflare_zone_id
  application_id = each.value.id
  name           = "Super Admins"
  precedence     = 1
  decision       = "allow"
  include {
    group = jsondecode(data.aws_ssm_parameter.cloudflare_super_admins_group_ids.value)
  }
}

resource "cloudflare_access_policy" "default_deny" {
  for_each       = cloudflare_access_application.default
  zone_id        = var.cloudflare_zone_id
  application_id = each.value.id
  name           = "Deny Public"
  precedence     = 100
  decision       = "deny"
  include {
    everyone = true
  }
}

resource "cloudflare_access_policy" "allow_users" {
  for_each = {
    for k, d in var.tunnels :
    k => d
    if contains(d.roles, "user")
  }

  zone_id        = var.cloudflare_zone_id
  application_id = cloudflare_access_application.default[each.key].id
  name           = "Users"
  decision       = "allow"
  precedence     = 2
  include {
    group = [
      var.cloudflare_client_user_group_id
    ]
  }
}
