locals {
  ssm_prefix = module.ssm_prefix.full_prefix
}

data "aws_region" "current" {}

module "ssm_prefix" {

  source            = "git::https://gitlab.com/guardianproject-ops/terraform-aws-ssm-param-store-iam?ref=tags/3.2.1"
  path_prefix       = "/${module.this.id}"
  prefix_with_label = false
  region            = data.aws_region.current.id
  context           = module.this.context
  kms_key_arn       = var.kms_key_arn
}

resource "aws_ssm_parameter" "cuwei_domain" {
  name  = "${local.ssm_prefix}/cuwei_domain"
  type  = "String"
  value = var.cuwei_domain
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "cuwei_backend_domain" {
  name  = "${local.ssm_prefix}/cuwei_backend_domain"
  type  = "String"
  value = var.cuwei_backend_domain
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "zammad_graphql_url" {
  name  = "${local.ssm_prefix}/zammad_graphql_url"
  type  = "String"
  value = var.cuwei_zammad_graphql_url
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "zammad_api_url" {
  name  = "${local.ssm_prefix}/zammad_api_url"
  type  = "String"
  value = var.cuwei_zammad_api_url
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "zammad_api_token" {
  name  = "${local.ssm_prefix}/zammad_api_token"
  type  = "SecureString"
  value = var.cuwei_zammad_api_token
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "cloudflare_audience" {
  name  = "${local.ssm_prefix}/cloudflare_audience"
  type  = "SecureString"
  value = var.cuwei_cloudflare_audience
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "cloudflare_url" {
  name  = "${local.ssm_prefix}/cloudflare_url"
  type  = "String"
  value = var.cuwei_cloudflare_url
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "cloudflare_client_id" {
  name  = "${local.ssm_prefix}/cloudflare_client_id"
  type  = "String"
  value = var.cuwei_cloudflare_client_id
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "cloudflare_client_secret" {
  name  = "${local.ssm_prefix}/cloudflare_client_secret"
  type  = "SecureString"
  value = var.cuwei_cloudflare_client_secret
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "docker_registry" {
  name  = "${local.ssm_prefix}/docker_registry"
  type  = "String"
  value = var.docker_registry
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "docker_registry_username" {
  name  = "${local.ssm_prefix}/docker_registry_username"
  type  = "String"
  value = var.docker_registry_username
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "docker_registry_password" {
  name  = "${local.ssm_prefix}/docker_registry_password"
  type  = "SecureString"
  value = var.docker_registry_password
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "docker_frontend_image" {
  name  = "${local.ssm_prefix}/docker_frontend_image"
  type  = "String"
  value = var.cuwei_docker_frontend_image
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "docker_backend_image" {
  name  = "${local.ssm_prefix}/docker_backend_image"
  type  = "String"
  value = var.cuwei_docker_backend_image
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "database_host" {
  name  = "${local.ssm_prefix}/database_host"
  type  = "String"
  value = var.cuwei_database_host
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "database_name" {
  name  = "${local.ssm_prefix}/database_name"
  type  = "String"
  value = var.cuwei_database_name
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "database_superuser_username" {
  name  = "${local.ssm_prefix}/database_superuser_username"
  type  = "String"
  value = var.cuwei_database_superuser_username
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "database_superuser_password" {
  name  = "${local.ssm_prefix}/database_superuser_password"
  type  = "SecureString"
  value = var.cuwei_database_superuser_password
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "database_username" {
  name  = "${local.ssm_prefix}/database_username"
  type  = "String"
  value = var.cuwei_database_username
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "database_password" {
  name  = "${local.ssm_prefix}/database_password"
  type  = "SecureString"
  value = var.cuwei_database_password
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "database_authenticator_username" {
  name  = "${local.ssm_prefix}/database_authenticator_username"
  type  = "String"
  value = var.cuwei_database_authenticator_username
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "database_authenticator_password" {
  name  = "${local.ssm_prefix}/database_authenticator_password"
  type  = "SecureString"
  value = var.cuwei_database_authenticator_password
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "database_visitor_username" {
  name  = "${local.ssm_prefix}/database_visitor_username"
  type  = "String"
  value = var.cuwei_database_visitor_username
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "database_admin_username" {
  name  = "${local.ssm_prefix}/database_admin_username"
  type  = "String"
  value = var.cuwei_database_admin_username
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "database_senior_analyst_username" {
  name  = "${local.ssm_prefix}/database_senior_analyst_username"
  type  = "String"
  value = var.cuwei_database_senior_analyst_username
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "database_investigator_username" {
  name  = "${local.ssm_prefix}/database_investigator_username"
  type  = "String"
  value = var.cuwei_database_investigator_username
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "database_analyst_username" {
  name  = "${local.ssm_prefix}/database_analyst_username"
  type  = "String"
  value = var.cuwei_database_analyst_username
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "database_anonymous_username" {
  name  = "${local.ssm_prefix}/database_anonymous_username"
  type  = "String"
  value = var.cuwei_database_anonymous_username
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "exports_bucket" {
  name  = "${local.ssm_prefix}/exports_bucket"
  type  = "String"
  value = var.cuwei_exports_bucket
  tags  = module.this.tags
}

locals {
  app_containers = {
    "backend" : "",
    "worker" : "",
    "frontend" : "",
    "nginx" : ""
  }
}

resource "aws_cloudwatch_log_group" "app" {
  for_each          = local.app_containers
  name              = "/${module.this.id}-${each.key}"
  retention_in_days = var.log_retention_days
}

resource "aws_ssm_parameter" "log_group_journald" {
  name      = "${local.ssm_prefix}/log_group_journald"
  value     = var.log_group_journald
  type      = "String"
  overwrite = true
}

resource "aws_ssm_parameter" "log_group_app" {
  for_each  = local.app_containers
  name      = "${local.ssm_prefix}/log_group_cuwei_${each.key}"
  value     = aws_cloudwatch_log_group.app[each.key].name
  type      = "String"
  overwrite = true
}

resource "aws_ssm_parameter" "cloudflare_account_id" {
  name      = "${local.ssm_prefix}/cloudflare_account_id"
  value     = var.cloudflare_account_id
  type      = "String"
  overwrite = true
}

resource "aws_ssm_parameter" "cloudflare_tunnel_name" {
  name      = "${local.ssm_prefix}/cloudflare_tunnel_name"
  value     = var.cloudflare_tunnel_name
  type      = "String"
  overwrite = true
}

resource "aws_ssm_parameter" "cloudflare_tunnel_id" {
  name      = "${local.ssm_prefix}/cloudflare_tunnel_id"
  value     = var.cloudflare_tunnel_id
  type      = "String"
  overwrite = true
}

resource "aws_ssm_parameter" "cloudflare_tunnel_secret" {
  name      = "${local.ssm_prefix}/cloudflare_tunnel_secret"
  value     = var.cloudflare_tunnel_secret
  type      = "SecureString"
  overwrite = true
}
