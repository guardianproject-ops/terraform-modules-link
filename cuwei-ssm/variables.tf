variable "log_retention_days" {
  type        = number
  description = "the number of days to retain logs in cloudwatch"
  default     = 14
}

variable "log_group_journald" {
  type        = string
  description = "the log group name for journald logs"
}

variable "aws_region" {
  type = string
}

variable "kms_key_arn" {
  type = string
}

variable "cuwei_domain" {
  type = string
}

variable "cuwei_backend_domain" {
  type = string
}

variable "cuwei_zammad_graphql_url" {
  type = string
}

variable "cuwei_zammad_api_url" {
  type = string
}

variable "cuwei_zammad_api_token" {
  type = string
}

variable "cuwei_cloudflare_audience" {
  type = string
}

variable "cuwei_cloudflare_url" {
  type = string
}

variable "cuwei_cloudflare_client_id" {
  type = string
}

variable "cuwei_cloudflare_client_secret" {
  type = string
}

variable "docker_registry" {
  type = string
}

variable "docker_registry_username" {
  type = string
}

variable "docker_registry_password" {
  type = string
}

variable "cuwei_docker_frontend_image" {
  type = string
}

variable "cuwei_docker_backend_image" {
  type = string
}

variable "cuwei_database_host" {
  type = string
}

variable "cuwei_database_name" {
  type = string
}

variable "cuwei_database_superuser_username" {
  type = string
}

variable "cuwei_database_superuser_password" {
  type = string
}

variable "cuwei_database_username" {
  type = string
}

variable "cuwei_database_password" {
  type = string
}

variable "cuwei_database_authenticator_username" {
  type = string
}

variable "cuwei_database_authenticator_password" {
  type = string
}

variable "cuwei_database_visitor_username" {
  type = string
}

variable "cuwei_database_admin_username" {
  type = string
}

variable "cuwei_database_senior_analyst_username" {
  type = string
}

variable "cuwei_database_investigator_username" {
  type = string
}

variable "cuwei_database_analyst_username" {
  type = string
}

variable "cuwei_database_anonymous_username" {
  type = string
}

variable "cuwei_exports_bucket" {
  type = string
}

variable "cloudflare_account_id" {
  type = string
}

variable "cloudflare_tunnel_name" {
  type = string
}

variable "cloudflare_tunnel_id" {
  type = string
}

variable "cloudflare_tunnel_secret" {
  type = string
}
