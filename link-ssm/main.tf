locals {
  ssm_prefix = module.ssm_prefix.full_prefix
}

data "aws_region" "current" {}

module "ssm_prefix" {

  source            = "git::https://gitlab.com/guardianproject-ops/terraform-aws-ssm-param-store-iam?ref=tags/3.2.4"
  path_prefix       = "/${module.this.id}"
  prefix_with_label = false
  region            = data.aws_region.current.id
  context           = module.this.context
  kms_key_arn       = var.kms_key_arn
}

resource "aws_ssm_parameter" "database_host" {
  for_each = var.databases
  name     = "${local.ssm_prefix}/${each.key}_db_host"
  type     = "String"
  value    = each.value.host
  tags     = module.this.tags
}

resource "aws_ssm_parameter" "database_port" {
  for_each = var.databases
  name     = "${local.ssm_prefix}/${each.key}_db_port"
  type     = "String"
  value    = each.value.port
  tags     = module.this.tags
}

resource "aws_ssm_parameter" "database_username" {
  for_each = var.databases
  name     = "${local.ssm_prefix}/${each.key}_db_username"
  type     = "String"
  value    = each.value.username
  tags     = module.this.tags
}

resource "aws_ssm_parameter" "database_password" {
  for_each = var.databases
  name     = "${local.ssm_prefix}/${each.key}_db_password"
  type     = "String"
  value    = each.value.password
  tags     = module.this.tags
}

resource "aws_ssm_parameter" "database_name" {
  for_each = var.databases
  name     = "${local.ssm_prefix}/${each.key}_db_name"
  type     = "String"
  value    = each.value.db
  tags     = module.this.tags
}

resource "aws_ssm_parameter" "database_uri" {
  for_each = var.databases
  name     = "${local.ssm_prefix}/${each.key}_db_uri"
  type     = "String"
  value    = each.value.uri
  tags     = module.this.tags
}

resource "aws_ssm_parameter" "docker_registry_enabled" {
  name  = "${local.ssm_prefix}/docker_registry_enabled"
  type  = "String"
  value = var.docker_registry_enabled
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "docker_registry" {
  count = var.docker_registry_enabled ? 1 : 0
  name  = "${local.ssm_prefix}/docker_registry"
  type  = "String"
  value = var.docker_registry
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "docker_registry_username" {
  count = var.docker_registry_enabled ? 1 : 0
  name  = "${local.ssm_prefix}/docker_registry_username"
  type  = "String"
  value = var.docker_registry_username
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "docker_registry_password" {
  count = var.docker_registry_enabled ? 1 : 0
  name  = "${local.ssm_prefix}/docker_registry_password"
  type  = "SecureString"
  value = var.docker_registry_password
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "zammad_docker_image" {
  name  = "${local.ssm_prefix}/zammad_docker_image"
  type  = "String"
  value = var.zammad_docker_image
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "zammad_domain" {
  name  = "${local.ssm_prefix}/zammad_domain"
  type  = "String"
  value = var.zammad_domain
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "zammad_graphql_cloudflare_access_audience" {
  name  = "${local.ssm_prefix}/zammad_graphql_cloudflare_access_audience"
  type  = "String"
  value = var.zammad_graphql_cloudflare_access_audience
  tags  = module.this.tags
  count = var.zammad_graphql_enabled ? 1 : 0
}

resource "aws_ssm_parameter" "zammad_graphql_cloudflare_access_url" {
  name  = "${local.ssm_prefix}/zammad_graphql_cloudflare_access_url"
  type  = "String"
  value = var.zammad_graphql_cloudflare_access_url
  tags  = module.this.tags
  count = var.zammad_graphql_enabled ? 1 : 0
}

resource "aws_ssm_parameter" "zammad_graphql_domain" {
  name  = "${local.ssm_prefix}/zammad_graphql_domain"
  type  = "String"
  value = var.zammad_graphql_domain
  tags  = module.this.tags
  count = var.zammad_graphql_enabled ? 1 : 0
}

resource "aws_ssm_parameter" "zammad_graphql_enabled" {
  name  = "${local.ssm_prefix}/zammad_graphql_enabled"
  type  = "String"
  value = var.zammad_graphql_enabled
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "zammad_monitoring_token" {
  name  = "${local.ssm_prefix}/zammad_monitoring_token"
  type  = "SecureString"
  value = var.zammad_monitoring_token
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "zammad_es_enabled" {
  name  = "${local.ssm_prefix}/zammad_es_enabled"
  type  = "String"
  value = var.zammad_es_enabled
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "zammad_es_host" {
  name  = "${local.ssm_prefix}/zammad_es_host"
  type  = "String"
  value = var.zammad_es_host
  tags  = module.this.tags
}
resource "aws_ssm_parameter" "zammad_es_port" {
  name  = "${local.ssm_prefix}/zammad_es_port"
  type  = "String"
  value = var.zammad_es_port
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "zammad_es_schema" {
  name  = "${local.ssm_prefix}/zammad_es_schema"
  type  = "String"
  value = var.zammad_es_schema
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "zammad_es_namespace" {
  name  = "${local.ssm_prefix}/zammad_es_namespace"
  type  = "String"
  value = var.zammad_es_namespace
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "zammad_signald_enabled" {
  name  = "${local.ssm_prefix}/zammad_signald_enabled"
  type  = "String"
  value = var.zammad_signald_enabled
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "zammad_protonmail_enabled" {
  name  = "${local.ssm_prefix}/zammad_protonmail_enabled"
  type  = "String"
  value = var.zammad_protonmail_enabled
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "label_studio_enabled" {
  name  = "${local.ssm_prefix}/label_studio_enabled"
  type  = "String"
  value = var.label_studio_enabled
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "label_studio_frontend_domain" {
  name  = "${local.ssm_prefix}/label_studio_frontend_domain"
  type  = "String"
  value = var.label_studio_frontend_domain
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "label_studio_docker_image" {
  name  = "${local.ssm_prefix}/label_studio_docker_image"
  type  = "String"
  value = var.label_studio_docker_image
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "metamigo_frontend_domain" {
  name  = "${local.ssm_prefix}/metamigo_frontend_domain"
  type  = "String"
  value = var.metamigo_frontend_domain
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "metamigo_api_domain" {
  name  = "${local.ssm_prefix}/metamigo_api_domain"
  type  = "String"
  value = var.metamigo_api_domain
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "database_authenticator_username" {
  name  = "${local.ssm_prefix}/database_authenticator_username"
  type  = "String"
  value = var.database_authenticator_username
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "database_authenticator_password" {
  name  = "${local.ssm_prefix}/database_authenticator_password"
  type  = "SecureString"
  value = var.database_authenticator_password
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "database_visitor_username" {
  name  = "${local.ssm_prefix}/database_visitor_username"
  type  = "String"
  value = var.database_visitor_username
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "metamigo_cloudflare_access_domain" {
  name  = "${local.ssm_prefix}/metamigo_cloudflare_access_domain"
  type  = "String"
  value = var.metamigo_cloudflare_access_domain
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "metamigo_cloudflare_access_audience" {
  name  = "${local.ssm_prefix}/metamigo_cloudflare_access_audience"
  type  = "String"
  value = var.metamigo_cloudflare_access_audience
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "metamigo_nextauth_secret" {
  name  = "${local.ssm_prefix}/nextauth_secret"
  type  = "SecureString"
  value = var.metamigo_nextauth_secret
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "metamigo_nextauth_audience" {
  name  = "${local.ssm_prefix}/nextauth_audience"
  type  = "String"
  value = var.metamigo_nextauth_audience
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "metamigo_nextauth_signing_key" {
  name  = "${local.ssm_prefix}/nextauth_signing_key"
  type  = "SecureString"
  value = var.metamigo_nextauth_signing_key
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "metamigo_nextauth_encryption_key" {
  name  = "${local.ssm_prefix}/nextauth_encryption_key"
  type  = "SecureString"
  value = var.metamigo_nextauth_encryption_key
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "google_id" {
  name  = "${local.ssm_prefix}/google_id"
  type  = "String"
  value = var.google_id
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "google_secret" {
  name  = "${local.ssm_prefix}/google_secret"
  type  = "SecureString"
  value = var.google_secret
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "leafcutter_enabled" {
  name  = "${local.ssm_prefix}/leafcutter_enabled"
  type  = "String"
  value = var.leafcutter_enabled
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "leafcutter_zammad_api_url" {
  name  = "${local.ssm_prefix}/leafcutter_zammad_api_url"
  type  = "String"
  value = var.leafcutter_zammad_api_url
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "leafcutter_zammad_api_key" {
  name  = "${local.ssm_prefix}/leafcutter_zammad_api_key"
  type  = "SecureString"
  value = var.leafcutter_zammad_api_key
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "leafcutter_label_studio_api_url" {
  name  = "${local.ssm_prefix}/leafcutter_label_studio_api_url"
  type  = "String"
  value = var.leafcutter_label_studio_api_url
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "leafcutter_label_studio_api_key" {
  name  = "${local.ssm_prefix}/leafcutter_label_studio_api_key"
  type  = "SecureString"
  value = var.leafcutter_label_studio_api_key
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "leafcutter_contributor_id" {
  name  = "${local.ssm_prefix}/leafcutter_contributor_id"
  type  = "String"
  value = var.leafcutter_contributor_id
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "leafcutter_contributor_name" {
  name  = "${local.ssm_prefix}/leafcutter_contributor_name"
  type  = "String"
  value = var.leafcutter_contributor_name
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "leafcutter_opensearch_api_url" {
  name  = "${local.ssm_prefix}/leafcutter_opensearch_api_url"
  type  = "String"
  value = var.leafcutter_opensearch_api_url
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "leafcutter_opensearch_username" {
  name  = "${local.ssm_prefix}/leafcutter_opensearch_username"
  type  = "String"
  value = var.leafcutter_opensearch_username
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "leafcutter_opensearch_password" {
  name  = "${local.ssm_prefix}/leafcutter_opensearch_password"
  type  = "SecureString"
  value = var.leafcutter_opensearch_password
  tags  = module.this.tags
}

locals {
  log_groups = {
    "zammad-railsserver" : "zammad_railsserver",
    "zammad-scheduler" : "zammad_scheduler",
    "zammad-websocket" : "zammad_websocket",
    "zammad-nginx" : "zammad_nginx",
    "zammad-graphql" : "zammad_graphql",
    "metamigo-api" : "metamigo_api",
    "metamigo-frontend" : "metamigo_frontend",
    "metamigo-worker" : "metamigo_worker",
    "cloudflared" : "cloudflared",
    "journald" : "journald",
  }
}

resource "aws_cloudwatch_log_group" "app" {
  for_each          = local.log_groups
  name              = "/${module.this.id}-${each.key}"
  retention_in_days = var.log_retention_days
}

resource "aws_ssm_parameter" "log_group_app" {
  for_each  = local.log_groups
  name      = "${local.ssm_prefix}/log_group_${each.value}"
  value     = aws_cloudwatch_log_group.app[each.key].name
  type      = "String"
  overwrite = true
}

resource "aws_ssm_parameter" "cloudflare_account_id" {
  name      = "${local.ssm_prefix}/cloudflare_account_id"
  value     = var.cloudflare_account_id
  type      = "String"
  overwrite = true
}

resource "aws_ssm_parameter" "cloudflare_tunnel_name" {
  name      = "${local.ssm_prefix}/cloudflare_tunnel_name"
  value     = var.cloudflare_tunnel_name
  type      = "String"
  overwrite = true
}

resource "aws_ssm_parameter" "cloudflare_tunnel_id" {
  name      = "${local.ssm_prefix}/cloudflare_tunnel_id"
  value     = var.cloudflare_tunnel_id
  type      = "String"
  overwrite = true
}

resource "aws_ssm_parameter" "cloudflare_tunnel_secret" {
  name      = "${local.ssm_prefix}/cloudflare_tunnel_secret"
  value     = var.cloudflare_tunnel_secret
  type      = "SecureString"
  overwrite = true
}

resource "aws_ssm_parameter" "metamigo_root_database_username" {
  name      = "${local.ssm_prefix}/metamigo_root_db_username"
  value     = var.metamigo_root_db_username
  type      = "String"
  overwrite = true
}

resource "aws_ssm_parameter" "metamigo_root_database_password" {
  name      = "${local.ssm_prefix}/metamigo_root_db_password"
  value     = var.metamigo_root_db_password
  type      = "SecureString"
  overwrite = true
}
