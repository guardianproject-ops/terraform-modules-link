variable "log_retention_days" {
  type        = number
  description = "the number of days to retain logs in cloudwatch"
  default     = 14
}

variable "log_group_journald" {
  type        = string
  description = "the log group name for journald logs"
}

variable "aws_region" {
  type = string
}

variable "kms_key_arn" {
  type = string
}

variable "docker_registry_enabled" {
  type    = bool
  default = false
}

variable "docker_registry" {
  type    = string
  default = ""
}

variable "docker_registry_username" {
  type    = string
  default = ""
}

variable "docker_registry_password" {
  type    = string
  default = ""
}

variable "zammad_docker_image" {
  type = string
}

variable "cloudflare_account_id" {
  type = string
}

variable "cloudflare_tunnel_name" {
  type = string
}

variable "cloudflare_tunnel_id" {
  type = string
}

variable "cloudflare_tunnel_secret" {
  type = string
}

variable "zammad_domain" {
  type = string
}

variable "zammad_graphql_cloudflare_access_audience" {
  type    = string
  default = ""
}

variable "zammad_graphql_cloudflare_access_url" {
  type    = string
  default = ""
}

variable "zammad_graphql_domain" {
  type    = string
  default = ""
}

variable "zammad_graphql_enabled" {
  type    = bool
  default = false
}

variable "zammad_monitoring_token" {
  type = string
}

variable "zammad_es_enabled" {
  type    = string
  default = false
}

variable "zammad_es_host" {
  type    = string
  default = " "
}

variable "zammad_es_port" {
  type    = number
  default = 443
}

variable "zammad_es_schema" {
  type    = string
  default = "https"
}

variable "zammad_es_namespace" {
  type    = string
  default = "zammad"
}

variable "zammad_signald_enabled" {
  type    = string
  default = false
}

variable "zammad_protonmail_enabled" {
  type    = string
  default = false
}

variable "label_studio_enabled" {
  type  = string
  default = false
}

variable "label_studio_frontend_domain" {
  type  = string
}

variable "label_studio_docker_image" {
  type  = string
}

variable "metamigo_frontend_domain" {
  type = string
}

variable "metamigo_api_domain" {
  type = string
}

variable "database_authenticator_username" {
  type = string
}

variable "database_authenticator_password" {
  type = string
}

variable "database_visitor_username" {
  type = string
}

variable "metamigo_cloudflare_access_domain" {
  type = string
}

variable "metamigo_cloudflare_access_audience" {
  type = string
}

variable "metamigo_nextauth_secret" {
  type = string
}

variable "metamigo_nextauth_audience" {
  type = string
}

variable "metamigo_nextauth_signing_key" {
  type = string
}

variable "metamigo_nextauth_encryption_key" {
  type = string
}

variable "google_id" {
  type = string
}

variable "google_secret" {
  type = string
}

variable "metamigo_root_db_username" {
  type = string
}

variable "metamigo_root_db_password" {
  type = string
}

variable "leafcutter_enabled" {
  type = string
  default = false
}

variable "leafcutter_zammad_api_url" {
  type = string
}

variable "leafcutter_zammad_api_key" {
  type = string
}

variable "leafcutter_label_studio_api_url" {
  type  = string
}

variable "leafcutter_label_studio_api_key" {
  type = string
}

variable "leafcutter_contributor_id" {
  type = string
}

variable "leafcutter_contributor_name" {
  type = string
}

variable "leafcutter_opensearch_api_url" {
  type = string
}

variable "leafcutter_opensearch_username" {
  type  = string
}

variable "leafcutter_opensearch_password" {
  type  = string
}

variable "databases" {
  type = map(object({ username = string, password = string, db = string, host = string, port = string, uri = string }))
  # add optional() here when we upgrade to terraform14
  #  type             = object({
  #    zammad_graphql = object({ username = string, password = string, db = string, host = string, port = string, uri = string }),
  #    zammad         = object({ username = string, password = string, db = string, host = string, port = string, uri = string }),
  #    cunei          = object({ username = string, password = string, db = string, host = string, port = string, uri = string }),
  #    cuwei          = object({ username = string, password = string, db = string, host = string, port = string, uri = string }),
  #    metamigo       = object({ username = string, password = string, db = string, host = string, port = string, uri = string }),
  #  })
}
