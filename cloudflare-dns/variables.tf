variable "cloudflare_edit_token" {
  type = string
}

variable "cloudflare_zone_id" {
  type = string
}

variable "cloudflare_account_id" {
  type = string
}

variable "tunnels" {
  type = map(object({
    domain = string
    tunnel = string
    name   = string
    roles  = list(string)
  }))
  description = "a map where the key is the argo tunnel id, and the value is a list of domains that should point to the tunnel with cnames."
}

variable "cloudflare_client_admin_group_id" {
  type = string
}

variable "cloudflare_client_submitter_group_id" {
  type = string
}

variable "cloudflare_client_agent_group_id" {
  type = string
}

variable "cloudflare_client_investigator_group_id" {
  type = string
}

variable "cloudflare_access_service_token_cunei_id" {
  type = string
}
