provider "cloudflare" {
  api_token = var.cloudflare_edit_token
}

resource "cloudflare_record" "tunnel_records" {
  for_each = var.tunnels
  zone_id  = var.cloudflare_zone_id
  name     = each.value.domain
  value    = "${each.value.tunnel}.cfargotunnel.com"
  proxied  = true
  type     = "CNAME"
}

resource "cloudflare_access_application" "default" {
  for_each                  = var.tunnels
  zone_id                   = var.cloudflare_zone_id
  name                      = each.value.name
  domain                    = each.value.domain
  session_duration          = "12h"
  auto_redirect_to_identity = true
}

resource "cloudflare_access_group" "gp_admins" {
  zone_id = var.cloudflare_zone_id
  name    = "Admins :: GP"
  include {
    email_domain = [
      "gpcmdln.net"
    ]
  }
}

resource "cloudflare_access_group" "cdr_admins" {
  zone_id = var.cloudflare_zone_id
  name    = "Admins :: CDR"
  include {
    email_domain = [
      "digiresilience.org",
      "okthanks.com"
    ]
  }
}

resource "cloudflare_access_policy" "allow_super_admins" {
  for_each       = cloudflare_access_application.default
  zone_id        = var.cloudflare_zone_id
  application_id = each.value.id
  name           = "Super Admins"
  precedence     = 1
  decision       = "allow"
  include {
    group = [
      cloudflare_access_group.gp_admins.id,
      cloudflare_access_group.cdr_admins.id,
    ]
  }
}

resource "cloudflare_access_policy" "default_deny" {
  for_each       = cloudflare_access_application.default
  zone_id        = var.cloudflare_zone_id
  application_id = each.value.id
  name           = "Deny Public"
  precedence     = 100
  decision       = "deny"
  include {
    everyone = true
  }
}


resource "cloudflare_access_policy" "allow_admins" {
  for_each = {
    for k, d in var.tunnels :
    k => d
    if contains(d.roles, "admin")
  }

  zone_id        = var.cloudflare_zone_id
  application_id = cloudflare_access_application.default[each.key].id
  name           = "Client Admins"
  decision       = "allow"
  precedence     = 2
  include {
    group = [
      var.cloudflare_client_admin_group_id
    ]
  }
}

resource "cloudflare_access_policy" "allow_agents" {
  for_each = {
    for k, d in var.tunnels :
    k => d
    if contains(d.roles, "agent")
  }

  zone_id        = var.cloudflare_zone_id
  application_id = cloudflare_access_application.default[each.key].id
  name           = "Agents"
  decision       = "allow"
  precedence     = 3
  include {
    group = [
      var.cloudflare_client_agent_group_id
    ]
  }
}


resource "cloudflare_access_policy" "allow_submitters" {
  for_each = {
    for k, d in var.tunnels :
    k => d
    if contains(d.roles, "submitter")
  }

  zone_id        = var.cloudflare_zone_id
  application_id = cloudflare_access_application.default[each.key].id
  name           = "Submitters"
  decision       = "allow"
  precedence     = 4
  include {
    group = [
      var.cloudflare_client_submitter_group_id
    ]
  }
}

resource "cloudflare_access_policy" "allow_investigators" {
  for_each = {
    for k, d in var.tunnels :
    k => d
    if contains(d.roles, "investigator")
  }

  zone_id        = var.cloudflare_zone_id
  application_id = cloudflare_access_application.default[each.key].id
  name           = "Investigators"
  decision       = "allow"
  precedence     = 5
  include {
    group = [
      var.cloudflare_client_submitter_group_id
    ]
  }
}

resource "cloudflare_access_policy" "allow_cunei" {
  for_each = {
    for k, d in var.tunnels :
    k => d
    if contains(d.roles, "cunei")
  }

  zone_id        = var.cloudflare_zone_id
  application_id = cloudflare_access_application.default[each.key].id
  name           = "Cunei Service Token"
  decision       = "non_identity"
  precedence     = 6
  include {
    service_token = [
      var.cloudflare_access_service_token_cunei_id
    ]
  }
}
