module "label" {
  source  = "cloudposse/label/null"
  version = "0.23.0"
  context = module.this.context
  name    = "link"
  tags    = merge(module.this.tags, { Application = "link" })
}



data "aws_region" "current" {}

data "aws_caller_identity" "current" {}

module "instance" {
  source                     = "../ec2-instance"
  is_prod_like               = var.is_prod_like
  ssm_prefix                 = var.ssm_prefix
  ssm_prefix_policy_arn      = var.ssm_prefix_policy_arn
  kms_key_arn                = var.kms_key_arn
  secretsmanager_ssh_key_id  = var.secretsmanager_ssh_key_id
  vpc_id                     = var.vpc_id
  vpc_cidr_block             = var.vpc_cidr_block
  availability_zone_1        = var.availability_zone_1
  subnet_id                  = var.subnet_id
  disk_allocation_gb         = var.disk_allocation_gb
  instance_type              = var.instance_type
  aws_key_pair_name_override = var.aws_key_pair_name_override
  extra_security_group_ids = var.zammad_es_enabled ? [
    var.elasticsearch_inbound_security_group_id
  ] : []
  ebs_volume_mount_path                    = var.ebs_volume_mount_path
  ebs_volume_disk_allocation_gb            = var.ebs_volume_disk_allocation_gb
  enable_ebs_volume                        = var.enable_ebs_volume
  enable_ebs_volume_after_initial_creation = var.enable_ebs_volume_after_initial_creation
  secure_metadata_endpoint                 = var.secure_metadata_endpoint
  context                                  = module.label.context
}
