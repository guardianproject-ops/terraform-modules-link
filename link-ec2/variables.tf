variable "elasticsearch_inbound_security_group_id" {
  type    = string
  default = ""
}

variable "zammad_es_enabled" {
  type    = string
  default = false
}

variable "secure_metadata_endpoint" {
  description = "Whether or not the metadata service should require the session token (IMDSv2). This is dangerous to apply to existing instances."
  type        = bool
  default     = false
}