variable "is_prod_like" {
  type        = bool
  description = "Flag to indicate this module is prod like or not. Some behavior will be different in prod like envs"
}
variable "server_username" {
  type        = string
  default     = "admin"
  description = "Username for the linux user used to login to the instances"
}
variable "tag_name" {
  type        = string
  default     = "RotateSSHKeys"
  description = "Tag name to locate the instances which should be rotated"
}
variable "tag_value" {
  type        = string
  default     = "true"
  description = "Tag value that must be set to locate the instances which should be rotated"
}
variable "ssh_key_rotate_after_days" {
  type        = number
  default     = 7
  description = "The ssh key will rotate every n days"
}

variable "ansible_source_dir" {
  type        = string
  description = "absolute path to the keanu ansible dir"
}
variable "ansible_out_dir" {
  type        = string
  description = "absolute path to where the ansible bundle should be stored"
}

variable "log_retention_days" {
  type        = number
  description = "the number of days to retain logs in cloudwatch"
  default     = 14
}

variable "vpc_subnet_ids" {
  description = "List of subnet ids when Lambda Function should run in the VPC. Usually private or intra subnets."
  type        = list(string)
  default     = null
}

variable "vpc_id" {
  description = "The VPC id that the lambda will be attached to"
  type        = string
  default     = null
}
