variable "log_retention_days" {
  type        = number
  description = "the number of days to retain logs in cloudwatch"
  default     = 14
}

variable "log_group_journald" {
  type        = string
  description = "the log group name for journald logs"
}

variable "aws_region" {
  type = string
}

variable "kms_key_arn" {
  type = string
}

variable "cunei_domain" {
  type = string
}

variable "cunei_admin_username" {
  type = string
}

variable "cunei_admin_password" {
  type = string
}

variable "cunei_cloudflare_audience" {
  type = string
}

variable "cunei_cloudflare_url" {
  type = string
}

variable "cunei_cloudflare_client_id" {
  type = string
}

variable "cunei_cloudflare_client_secret" {
  type = string
}

variable "cunei_secrets" {
  type = string
}

variable "cunei_zammad_group" {
  type = string
}

variable "cunei_zammad_api_url" {
  type = string
}

variable "cunei_zammad_token" {
  type = string
}

variable "docker_registry" {
  type = string
}

variable "docker_registry_username" {
  type = string
}

variable "docker_registry_password" {
  type = string
}

variable "docker_image" {
  type = string
}

variable "cunei_wayback_api_url" {
  type = string
}

variable "cunei_wayback_access_key" {
  type = string
}

variable "cunei_wayback_secret_key" {
  type = string
}

variable "cunei_perma_api_url" {
  type = string
}

variable "cunei_perma_api_key" {
  type = string
}

variable "cloudflare_account_id" {
  type = string
}

variable "cloudflare_tunnel_name" {
  type = string
}

variable "cloudflare_tunnel_id" {
  type = string
}

variable "cloudflare_tunnel_secret" {
  type = string
}
