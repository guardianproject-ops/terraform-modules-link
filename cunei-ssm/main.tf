locals {
  ssm_prefix = module.ssm_prefix.full_prefix
}

data "aws_region" "current" {}

module "ssm_prefix" {

  source            = "git::https://gitlab.com/guardianproject-ops/terraform-aws-ssm-param-store-iam?ref=tags/3.2.1"
  path_prefix       = "/${module.this.id}"
  prefix_with_label = false
  region            = data.aws_region.current.id
  context           = module.this.context
  kms_key_arn       = var.kms_key_arn
}

resource "aws_ssm_parameter" "cunei_domain" {
  name  = "${local.ssm_prefix}/cunei_domain"
  type  = "String"
  value = var.cunei_domain
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "cunei_admin_username" {
  name  = "${local.ssm_prefix}/cunei_admin_username"
  type  = "String"
  value = var.cunei_admin_username
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "cunei_admin_password" {
  name  = "${local.ssm_prefix}/cunei_admin_password"
  type  = "SecureString"
  value = var.cunei_admin_password
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "cloudflare_audience" {
  name  = "${local.ssm_prefix}/cloudflare_audience"
  type  = "SecureString"
  value = var.cunei_cloudflare_audience
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "cloudflare_url" {
  name  = "${local.ssm_prefix}/cloudflare_url"
  type  = "String"
  value = var.cunei_cloudflare_url
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "cloudflare_client_id" {
  name  = "${local.ssm_prefix}/cloudflare_client_id"
  type  = "String"
  value = var.cunei_cloudflare_client_id
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "cloudflare_client_secret" {
  name  = "${local.ssm_prefix}/cloudflare_client_secret"
  type  = "SecureString"
  value = var.cunei_cloudflare_client_secret
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "cunei_secrets" {
  name  = "${local.ssm_prefix}/cunei_secrets"
  type  = "SecureString"
  value = var.cunei_secrets
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "zammad_group" {
  name  = "${local.ssm_prefix}/zammad_group"
  type  = "String"
  value = var.cunei_zammad_group
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "zammad_api_url" {
  name  = "${local.ssm_prefix}/zammad_api_url"
  type  = "String"
  value = var.cunei_zammad_api_url
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "zammad_token" {
  name  = "${local.ssm_prefix}/zammad_token"
  type  = "SecureString"
  value = var.cunei_zammad_token
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "docker_registry" {
  name  = "${local.ssm_prefix}/docker_registry"
  type  = "String"
  value = var.docker_registry
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "docker_registry_username" {
  name  = "${local.ssm_prefix}/docker_registry_username"
  type  = "String"
  value = var.docker_registry_username
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "docker_registry_password" {
  name  = "${local.ssm_prefix}/docker_registry_password"
  type  = "SecureString"
  value = var.docker_registry_password
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "docker_image" {
  name  = "${local.ssm_prefix}/docker_image"
  type  = "String"
  value = var.docker_image
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "wayback_api" {
  name  = "${local.ssm_prefix}/wayback_api_url"
  type  = "String"
  value = var.cunei_wayback_api_url
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "wayback_access_key" {
  name  = "${local.ssm_prefix}/wayback_access_key"
  type  = "SecureString"
  value = var.cunei_wayback_access_key
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "wayback_secret_key" {
  name  = "${local.ssm_prefix}/wayback_secret_key"
  type  = "SecureString"
  value = var.cunei_wayback_secret_key
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "perma_api" {
  name  = "${local.ssm_prefix}/perma_api_url"
  type  = "String"
  value = var.cunei_perma_api_url
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "perma_api_key" {
  name  = "${local.ssm_prefix}/perma_api_key"
  type  = "SecureString"
  value = var.cunei_perma_api_key
  tags  = module.this.tags
}

locals {
  app_containers = {
    "backend" : "",
    "worker" : "",
    "redis" : "",
  }
}

resource "aws_cloudwatch_log_group" "app" {
  for_each          = local.app_containers
  name              = "/${module.this.id}-${each.key}"
  retention_in_days = var.log_retention_days
}

resource "aws_ssm_parameter" "log_group_journald" {
  name      = "${local.ssm_prefix}/log_group_journald"
  value     = var.log_group_journald
  type      = "String"
  overwrite = true
}

resource "aws_ssm_parameter" "log_group_app" {
  for_each  = local.app_containers
  name      = "${local.ssm_prefix}/log_group_cunei_${each.key}"
  value     = aws_cloudwatch_log_group.app[each.key].name
  type      = "String"
  overwrite = true
}

resource "aws_ssm_parameter" "cloudflare_account_id" {
  name      = "${local.ssm_prefix}/cloudflare_account_id"
  value     = var.cloudflare_account_id
  type      = "String"
  overwrite = true
}

resource "aws_ssm_parameter" "cloudflare_tunnel_name" {
  name      = "${local.ssm_prefix}/cloudflare_tunnel_name"
  value     = var.cloudflare_tunnel_name
  type      = "String"
  overwrite = true
}

resource "aws_ssm_parameter" "cloudflare_tunnel_id" {
  name      = "${local.ssm_prefix}/cloudflare_tunnel_id"
  value     = var.cloudflare_tunnel_id
  type      = "String"
  overwrite = true
}

resource "aws_ssm_parameter" "cloudflare_tunnel_secret" {
  name      = "${local.ssm_prefix}/cloudflare_tunnel_secret"
  value     = var.cloudflare_tunnel_secret
  type      = "SecureString"
  overwrite = true
}
