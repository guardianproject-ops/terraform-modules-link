# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## 2.0.0 (2021-06-16)


### ⚠ BREAKING CHANGES

* BREAKING! replace multi-db role with plain rds

### Bug Fixes

* replace session manager module with a single AWS policy ([6cd793f](https://gitlab.com/guardianproject-ops/terraform-modules-link/commit/6cd793f5d9a7b8e6b78dd3a57a05297dd92a235d))
* update ssh rotation lambda ([2535472](https://gitlab.com/guardianproject-ops/terraform-modules-link/commit/2535472bf37a4479606c28ee7b46285bbdeb6b20))


### rds

* BREAKING! replace multi-db role with plain rds ([b49789c](https://gitlab.com/guardianproject-ops/terraform-modules-link/commit/b49789c2566949849e2d74378a3d6995c2fb2bf0))
