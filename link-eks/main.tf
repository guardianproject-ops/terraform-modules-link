data "aws_vpc" "default" {
  tags = {
    "Name" : "aws-controltower-VPC"
  }
}

data "aws_subnet" "private1" {
  vpc_id = data.aws_vpc.default.id

  tags = {
    "Name" : "aws-controltower-PrivateSubnet1A"
  }
}

data "aws_subnet" "private2" {
  vpc_id = data.aws_vpc.default.id

  tags = {
    "Name" : "aws-controltower-PrivateSubnet2A"
  }
}


provider "helm" {
  kubernetes {
    host                   = var.cluster_endpoint
    cluster_ca_certificate = base64decode(var.cluster_ca_certificate)
    exec {
      api_version = "client.authentication.k8s.io/v1alpha1"
      args        = ["eks", "get-token", "--cluster-name", var.cluster_name]
      command     = "aws"
    }
  }
}

provider "kubernetes" {
  host                   = var.cluster_endpoint
  cluster_ca_certificate = base64decode(var.cluster_ca_certificate)
  exec {
    api_version = "client.authentication.k8s.io/v1alpha1"
    args        = ["eks", "get-token", "--cluster-name", var.cluster_name]
    command     = "aws"
  }
}

resource "kubernetes_namespace" "zammad" {
  metadata {
    name = "zammad"
  }
}

resource "aws_efs_file_system" "zammad" {
  creation_token = "zammad"
}

resource "kubernetes_persistent_volume" "zammad" {
  metadata {
    name = "zammad"
  }
  spec {
    capacity = {
      storage = "20Gi"
    }
    volume_mode                      = "Filesystem"
    access_modes                     = ["ReadWriteMany"]
    persistent_volume_reclaim_policy = "Retain"
    storage_class_name               = "efs-sc"
    persistent_volume_source {
      csi {
        driver        = "efs.csi.aws.com"
        volume_handle = aws_efs_file_system.zammad.id
      }
    }
  }
}

resource "kubernetes_persistent_volume_claim" "zammad" {
  metadata {
    name      = "zammad"
    namespace = "zammad"
  }
  spec {
    access_modes       = ["ReadWriteMany"]
    storage_class_name = "efs-sc"
    resources {
      requests = {
        storage = "20Gi"
      }
    }
  }
}

resource "helm_release" "zammad" {
  name = "zammad"

  repository = "https://zammad.github.io/zammad-helm"
  chart      = "zammad"
  namespace  = kubernetes_namespace.zammad.metadata[0].name
  timeout    = 600

  set {
    name  = "postgresql.enabled"
    value = false
  }

  set {
    name  = "elasticsearch.enabled"
    value = false
  }

  set {
    name  = "persistence.existingClaim"
    value = kubernetes_persistent_volume_claim.zammad.metadata[0].name
  }

  set {
    name  = "resources.requests.cpu"
    value = "2000m"
  }

  set {
    name  = "resources.requests.memory"
    value = "4000Mi"
  }

  set {
    name  = "tolerations[0].key"
    value = "eks.amazonaws.com/compute-type"
  }

  set {
    name  = "tolerations[0].operator"
    value = "Equal"
  }

  set {
    name  = "tolerations[0].value"
    value = "fargate"
  }

  set {
    name  = "tolerations[0].effect"
    value = "NoSchedule"
  }

  set {
    name  = "ingress.enabled"
    value = true
  }

  set {
    name  = "ingress.annotations.kubernetes\\.io/ingress\\.class"
    value = "alb"
  }

  set {
    name  = "ingress.annotations.alb\\.ingress\\.kubernetes\\.io\\/scheme"
    value = "internet-facing"
  }

  set {
    name  = "ingress.annotations.alb\\.ingress\\.kubernetes\\.io\\/target-type"
    value = "ip"
  }

  set {
    name  = "ingress.annotations.alb\\.ingress\\.kubernetes\\.io\\/success-codes"
    value = "200\\,302"
  }

  set {
    name  = "ingress.annotations.alb\\.ingress\\.kubernetes\\.io\\/listen-ports"
    value = "[{\"HTTPS\": 443}]"
  }

  set {
    name  = "ingress.annotations.alb\\.ingress\\.kubernetes\\.io\\/backend-protocol"
    value = "HTTP"
  }
}

module "eks_subnet_label" {
  source     = "cloudposse/label/null"
  version    = "0.22.1"
  attributes = ["eks"]
  tags = {
    "Visibility"             = "public",
    "Application"            = "eks",
    "kubernetes.io/role/elb" = "1"
  }
  context = module.this.context
}

resource "aws_subnet" "eks_public1" {
  vpc_id                  = data.aws_vpc.default.id
  cidr_block              = "10.57.3.0/24" # make into a variable
  availability_zone       = data.aws_subnet.private1.availability_zone
  map_public_ip_on_launch = true

  tags = merge(
    module.eks_subnet_label.tags,
    {
      "Name" = format(
        "%s%s%s",
        module.eks_subnet_label.id,
        module.this.delimiter,
        replace(
          data.aws_subnet.private1.availability_zone,
          "-",
          module.this.delimiter
        )
      )
    }
  )

  lifecycle {
    ignore_changes = [tags.Visibility]
  }
}

resource "aws_subnet" "eks_public2" {
  vpc_id                  = data.aws_vpc.default.id
  cidr_block              = "10.57.4.0/24" # make into a variable
  availability_zone       = data.aws_subnet.private2.availability_zone
  map_public_ip_on_launch = true

  tags = merge(
    module.eks_subnet_label.tags,
    {
      "Name" = format(
        "%s%s%s",
        module.eks_subnet_label.id,
        module.this.delimiter,
        replace(
          data.aws_subnet.private2.availability_zone,
          "-",
          module.this.delimiter
        )
      )
    }
  )

  lifecycle {
    ignore_changes = [tags.Visibility]
  }
}

data "aws_internet_gateway" "default" {
  tags = {
    "Name" : "this-needs-a-value"
  }
}

resource "aws_route" "eks_subnet" {
  route_table_id         = data.aws_vpc.default.main_route_table_id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = data.aws_internet_gateway.default.id
}

resource "aws_route_table_association" "eks_subnet1" {
  subnet_id      = aws_subnet.eks_public1.id
  route_table_id = data.aws_vpc.default.main_route_table_id
}

resource "aws_route_table_association" "eks_subnet2" {
  subnet_id      = aws_subnet.eks_public2.id
  route_table_id = data.aws_vpc.default.main_route_table_id
}

