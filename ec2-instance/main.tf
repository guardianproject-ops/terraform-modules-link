data "aws_region" "current" {}
data "aws_caller_identity" "current" {}

locals {
  region      = data.aws_region.current.name
  account_id  = data.aws_caller_identity.current.account_id
  ssh_key_pub = jsondecode(data.aws_secretsmanager_secret_version.ssh_key.secret_string)["PublicKey"]
  default_ami_filter = {
    "tag:Application" : "debian-base",
    "tag:Namespace" : module.this.namespace,
    "tag:Environment" : module.this.environment,
    "tag:Kind" : "gold",
  }
  ami_filter = length(var.ami_filter) == 0 ? local.default_ami_filter : var.ami_filter
  ami        = coalesce(var.ami, data.aws_ami.default.id)
}

data "aws_secretsmanager_secret_version" "ssh_key" {
  secret_id = var.secretsmanager_ssh_key_id
}

resource "aws_key_pair" "current_ssh_key" {
  count      = length(var.aws_key_pair_name_override) == 0 ? 1 : 0
  key_name   = "${module.this.id}-INITIAL-KEY-ROTATE-ME"
  public_key = local.ssh_key_pub
}

data "aws_ami" "default" {
  most_recent = true
  owners      = var.ami_owners

  dynamic "filter" {
    for_each = local.ami_filter

    content {
      name   = filter.key
      values = [filter.value]
    }
  }
}

data "aws_iam_policy_document" "send_logs_to_cloudwatch" {
  statement {
    sid = "AllowCloudwatchLogsList"
    actions = [
      "logs:DescribeLogGroups",
    ]

    resources = [
      "arn:aws:logs:${local.region}:${local.account_id}:log-group*"
    ]
  }
  statement {
    sid = "AllowCloudwatchLogsAccess"
    actions = [
      "logs:CreateLogStream",
      "logs:PutLogEvents",
      "logs:DescribeLogStreams",
    ]

    resources = [
      "arn:aws:logs:${local.region}:${local.account_id}:log-group:${var.ssm_prefix}*"
    ]
  }
}

resource "aws_iam_policy" "send_logs_to_cloudwatch" {
  name        = "session-manager-with-logs-${module.this.id}"
  description = "Policy that allows instance to send logs to cloudwatch"
  policy      = data.aws_iam_policy_document.send_logs_to_cloudwatch.json
}

module "instance_role_attachment" {
  source  = "git::https://gitlab.com/guardianproject-ops/terraform-aws-iam-instance-role-policy-attachment?ref=tags/2.1.1"
  context = module.this.context

  iam_policy_arns = concat(var.extra_iam_policy_arns, [
    var.ssm_prefix_policy_arn,
    "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore",
    aws_iam_policy.send_logs_to_cloudwatch.arn
  ])
}

resource "aws_iam_instance_profile" "profile" {
  name = module.this.id
  role = module.instance_role_attachment.instance_role_id
}

locals {
  default_user_data    = <<EOF
#cloud-config

bootcmd:
  - 'while [ `lsblk | wc -l` -lt 4 ]; do echo waiting on disks...; sleep 5; done; lsblk; sleep 5; lsblk; lsblk --pairs --output NAME,TYPE,FSTYPE,LABEL /dev/nvme1n1;'

mounts:
  - [ "LABEL=state", "${var.ebs_volume_mount_path}" ]

disk_setup:
  /dev/nvme1n1:
    layout:
      - 100
    overwrite: false
    table_type: 'gpt'

fs_setup:
  -   device: /dev/nvme1n1
      partition: any
      label: state
      filesystem: ext4
      overwrite: false

EOF
  calculated_user_data = var.enable_ebs_volume ? local.default_user_data : ""
  user_data            = var.enable_ebs_volume_after_initial_creation ? "" : local.calculated_user_data
}


resource "aws_instance" "default" {
  ami                  = local.ami
  instance_type        = var.instance_type
  subnet_id            = var.subnet_id
  key_name             = length(var.aws_key_pair_name_override) == 0 ? aws_key_pair.current_ssh_key[0].key_name : var.aws_key_pair_name_override
  monitoring           = true
  availability_zone    = var.availability_zone_1
  user_data            = local.user_data
  iam_instance_profile = aws_iam_instance_profile.profile.id

  root_block_device {
    volume_type = "gp2"
    volume_size = var.disk_allocation_gb
    encrypted   = true
    kms_key_id  = var.kms_key_arn
  }

  metadata_options {
    http_tokens = var.secure_metadata_endpoint ? "required" : "optional"
  }

  disable_api_termination = var.is_prod_like

  vpc_security_group_ids = concat(var.extra_security_group_ids,
  [aws_security_group.default.id, aws_security_group.ssh_rotate.id])

  # this prevents changes to the ami from recreating the whole instance
  lifecycle {
    ignore_changes = ["ami"]
  }

  tags = merge(module.this.tags,
    {
      "RotateSSHKeys" : "true",
      "SSMPrefix" : var.ssm_prefix,
      "Name" : module.this.id
  })
}

resource "aws_security_group" "default" {
  name   = module.this.id
  vpc_id = var.vpc_id

  ingress {
    from_port   = var.node_exporter_port
    to_port     = var.node_exporter_port
    protocol    = "tcp"
    cidr_blocks = [var.vpc_cidr_block]
  }

  ingress {
    from_port   = var.cloudflared_metrics_port
    to_port     = var.cloudflared_metrics_port
    protocol    = "tcp"
    cidr_blocks = [var.vpc_cidr_block]
  }


  dynamic "ingress" {
    for_each = var.ingress
    content {
      from_port   = ingress.value["from_port"]
      to_port     = ingress.value["to_port"]
      protocol    = ingress.value["protocol"]
      cidr_blocks = [var.vpc_cidr_block]
    }
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = module.this.tags
}

resource "aws_ebs_volume" "default" {
  count             = var.enable_ebs_volume ? 1 : 0
  availability_zone = var.availability_zone_1
  size              = var.ebs_volume_disk_allocation_gb
  encrypted         = true
  kms_key_id        = var.kms_key_arn
  tags              = module.this.tags
}

resource "aws_volume_attachment" "default" {
  count = var.enable_ebs_volume ? 1 : 0

  device_name = "/dev/sdh"
  volume_id   = aws_ebs_volume.default[0].id
  instance_id = aws_instance.default.id
}

data "aws_ssm_parameter" "ssh_rotate_lambda_security_group_id" {
  name = "/region-settings/ssh_rotate_lambda_security_group_id"
}

resource "aws_security_group" "ssh_rotate" {
  name   = "${module.this.id}-ssh-rotate"
  vpc_id = var.vpc_id

  ingress {
    from_port = 22
    to_port   = 22
    protocol  = "tcp"
    security_groups = [
      data.aws_ssm_parameter.ssh_rotate_lambda_security_group_id.value
    ]
  }

  tags = module.this.tags
}
