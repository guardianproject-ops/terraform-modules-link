data "aws_vpc" "default" {
  tags = {
    "Name" : "aws-controltower-VPC"
  }
}

data "aws_subnet" "private1" {
  vpc_id = data.aws_vpc.default.id

  tags = {
    "Name" : "aws-controltower-PrivateSubnet1A"
  }
}

data "aws_subnet" "private2" {
  vpc_id = data.aws_vpc.default.id

  tags = {
    "Name" : "aws-controltower-PrivateSubnet2A"
  }
}


provider "helm" {
  kubernetes {
    host                   = var.cluster_endpoint
    cluster_ca_certificate = base64decode(var.cluster_ca_certificate)
    exec {
      api_version = "client.authentication.k8s.io/v1alpha1"
      args        = ["eks", "get-token", "--cluster-name", var.cluster_name]
      command     = "aws"
    }
  }
}

provider "kubernetes" {
  host                   = var.cluster_endpoint
  cluster_ca_certificate = base64decode(var.cluster_ca_certificate)
  exec {
    api_version = "client.authentication.k8s.io/v1alpha1"
    args        = ["eks", "get-token", "--cluster-name", var.cluster_name]
    command     = "aws"
  }
}

resource "kubernetes_namespace" "airflow" {
  metadata {
    name = "airflow"
  }
}

resource "helm_release" "airflow" {
  name = "airflow"

  repository = "https://airflow.apache.org"
  chart      = "airflow"
  namespace  = kubernetes_namespace.airflow.metadata[0].name
  timeout    = 600

  set {
    name  = "executor"
    value = "KubernetesExecutor"
  }

  set {
    name  = "resources.requests.cpu"
    value = "2000m"
  }

  set {
    name  = "resources.requests.memory"
    value = "4000Mi"
  }

  set {
    name  = "tolerations[0].key"
    value = "eks.amazonaws.com/compute-type"
  }

  set {
    name  = "tolerations[0].operator"
    value = "Equal"
  }

  set {
    name  = "tolerations[0].value"
    value = "fargate"
  }

  set {
    name  = "tolerations[0].effect"
    value = "NoSchedule"
  }
}

module "eks_subnet_label" {
  source     = "cloudposse/label/null"
  version    = "0.22.1"
  attributes = ["eks"]
  tags = {
    "Visibility"             = "public",
    "Application"            = "eks",
    "kubernetes.io/role/elb" = "1"
  }
  context = module.this.context
}

resource "aws_subnet" "eks_public1" {
  vpc_id                  = data.aws_vpc.default.id
  cidr_block              = "10.57.3.0/24" # make into a variable
  availability_zone       = data.aws_subnet.private1.availability_zone
  map_public_ip_on_launch = true

  tags = merge(
    module.eks_subnet_label.tags,
    {
      "Name" = format(
        "%s%s%s",
        module.eks_subnet_label.id,
        module.this.delimiter,
        replace(
          data.aws_subnet.private1.availability_zone,
          "-",
          module.this.delimiter
        )
      )
    }
  )

  lifecycle {
    ignore_changes = [tags.Visibility]
  }
}

resource "aws_subnet" "eks_public2" {
  vpc_id                  = data.aws_vpc.default.id
  cidr_block              = "10.57.4.0/24" # make into a variable
  availability_zone       = data.aws_subnet.private2.availability_zone
  map_public_ip_on_launch = true

  tags = merge(
    module.eks_subnet_label.tags,
    {
      "Name" = format(
        "%s%s%s",
        module.eks_subnet_label.id,
        module.this.delimiter,
        replace(
          data.aws_subnet.private2.availability_zone,
          "-",
          module.this.delimiter
        )
      )
    }
  )

  lifecycle {
    ignore_changes = [tags.Visibility]
  }
}

data "aws_internet_gateway" "default" {
  tags = {
    "Name" : "this-needs-a-value"
  }
}

resource "aws_route" "eks_subnet" {
  route_table_id         = data.aws_vpc.default.main_route_table_id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = data.aws_internet_gateway.default.id
}

resource "aws_route_table_association" "eks_subnet1" {
  subnet_id      = aws_subnet.eks_public1.id
  route_table_id = data.aws_vpc.default.main_route_table_id
}

resource "aws_route_table_association" "eks_subnet2" {
  subnet_id      = aws_subnet.eks_public2.id
  route_table_id = data.aws_vpc.default.main_route_table_id
}

