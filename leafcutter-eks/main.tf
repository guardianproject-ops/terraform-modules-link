data "aws_vpc" "default" {
  tags = {
    "Name" : "aws-controltower-VPC"
  }
}

data "aws_subnet" "private1" {
  vpc_id = data.aws_vpc.default.id

  tags = {
    "Name" : "aws-controltower-PrivateSubnet1A"
  }
}

data "aws_subnet" "private2" {
  vpc_id = data.aws_vpc.default.id

  tags = {
    "Name" : "aws-controltower-PrivateSubnet2A"
  }
}

provider "helm" {
  kubernetes {
    host                   = var.cluster_endpoint
    cluster_ca_certificate = base64decode(var.cluster_ca_certificate)
    exec {
      api_version = "client.authentication.k8s.io/v1alpha1"
      args        = ["eks", "get-token", "--cluster-name", var.cluster_name]
      command     = "aws"
    }
  }
}

provider "kubernetes" {
  host                   = var.cluster_endpoint
  cluster_ca_certificate = base64decode(var.cluster_ca_certificate)
  exec {
    api_version = "client.authentication.k8s.io/v1alpha1"
    args        = ["eks", "get-token", "--cluster-name", var.cluster_name]
    command     = "aws"
  }
}

resource "kubernetes_namespace" "leafcutter" {
  metadata {
    name = "leafcutter"
  }
}

resource "aws_efs_file_system" "opensearch" {
  creation_token = "opensearch"
  encrypted      = true
  kms_key_id     = var.kms_key_arn
}

resource "aws_efs_access_point" "opensearch" {
  file_system_id = aws_efs_file_system.opensearch.id
  posix_user {
    gid = 1000
    uid = 1000
  }
  root_directory {
    path = "/data"
    creation_info {
      owner_gid   = 1000
      owner_uid   = 1000
      permissions = 755
    }
  }
}

resource "aws_efs_mount_target" "private1" {
  file_system_id = aws_efs_file_system.opensearch.id
  subnet_id      = data.aws_subnet.private1.id
}

resource "aws_efs_mount_target" "private2" {
  file_system_id = aws_efs_file_system.opensearch.id
  subnet_id      = data.aws_subnet.private2.id
}

resource "kubernetes_persistent_volume" "opensearch" {
  metadata {
    name = "opensearch"
  }
  spec {
    capacity = {
      storage = "20Gi"
    }
    volume_mode                      = "Filesystem"
    access_modes                     = ["ReadWriteMany"]
    persistent_volume_reclaim_policy = "Retain"
    storage_class_name               = "efs-sc"
    persistent_volume_source {
      csi {
        driver        = "efs.csi.aws.com"
        volume_handle = "${aws_efs_file_system.opensearch.id}::${aws_efs_access_point.opensearch.id}"
      }
    }
  }
}

resource "kubernetes_persistent_volume_claim" "opensearch" {
  metadata {
    name      = "opensearch-cluster-master"
    namespace = "leafcutter"
  }
  spec {
    access_modes       = ["ReadWriteMany"]
    storage_class_name = "efs-sc"
    resources {
      requests = {
        storage = "20Gi"
      }
    }
  }
}

resource "kubernetes_secret" "leafcutter_web_docker_config" {
  metadata {
    name      = "leafcutter-web-docker-config"
    namespace = "leafcutter"
  }

  data = {
    ".dockerconfigjson" = var.leafcutter_registry_docker_config
  }

  type = "kubernetes.io/dockerconfigjson"
}

resource "kubernetes_secret" "opensearch_security_plugin_config" {
  metadata {
    name      = "opensearch-security-plugin-config"
    namespace = "leafcutter"
  }

  data = {
    "config.yml" = yamlencode({
      _meta = {
        type = "config"
         config_version = 2
      }

      config = {
      dynamic = {
        authc = {
          proxy_auth_domain = {
            http_enabled = true
            transport_enabled = true
            order = 0
            http_authenticator = {
              type = "proxy"
              challenge = false
              config = {
                user_header = "x-proxy-user"
                user_roles = "x-proxy-roles"
              }
            }
            authentication_backend = {
              type = "noop"
            }
          }
        }
        http = {
          anonymous_auth_enabled = false
          xff = {
            enabled = true
            remoteIpHeader = "x-forwarded-for"
            internalProxies = ".*"
          }
        }
      }
}
    })
}


  type = "Opaque"
}

resource "helm_release" "opensearch" {
  name      = "opensearch"
  # repository = "https://opensearch-project.github.io/helm-charts/"
  # chart      = "opensearch"
  # version    = "1.2.5"
  chart     = "helm-charts/charts/opensearch/"
  namespace = kubernetes_namespace.leafcutter.metadata[0].name
  timeout   = 600

  set {
    name  = "imageTag"
    value = var.opensearch_version
  }

  set {
    name  = "sysctl.enabled"
    value = false
  }

  set {
    name  = "replicas"
    value = 1
  }

  set {
    name = "mininumMasterNodes"
    value = 0
  }

  set {
    name  = "persistence.enabled"
    value = false
  }

   set {
    name = "securityConfig.securityConfigSecret"
    value = kubernetes_secret.opensearch_security_plugin_config.metadata[0].name
  }

  set {
    name  = "config.opensearch\\.yml.node.store.allow_mmap"
    value = false
  }

  set {
    name  = "config.opensearch\\.yml.discovery.type"
    value = "single-node"
  }
/*
   set {
    name  = "config.opensearch\\.yml.plugins.security.disabled"
    value = "true"
  }
 */
  set {
    name  = "extraVolumes[0].name"
    value = kubernetes_persistent_volume.opensearch.metadata[0].name
  }

  set {
    name  = "extraVolumes[0].persistentVolumeClaim.claimName"
    value = kubernetes_persistent_volume_claim.opensearch.metadata[0].name
  }

  set {
    name  = "extraVolumeMounts[0].name"
    value = kubernetes_persistent_volume.opensearch.metadata[0].name
  }

  set {
    name  = "extraVolumeMounts[0].mountPath"
    value = "/usr/share/opensearch/data"
  }

  set {
    name  = "opensearchJavaOpts"
    value = "-Xmx3G -Xms3G"
  }

  set {
    name  = "resources.requests.cpu"
    value = "4000m"
  }

  set {
    name  = "resources.requests.memory"
    value = "4000Mi"
  }

  set {
    name  = "tolerations[0].key"
    value = "eks.amazonaws.com/compute-type"
  }

  set {
    name  = "tolerations[0].operator"
    value = "Equal"
  }

  set {
    name  = "tolerations[0].value"
    value = "fargate"
  }

  set {
    name  = "tolerations[0].effect"
    value = "NoSchedule"
  }
}

resource "helm_release" "opensearch_dashboards" {
  name = "opensearch-dashboards-1"

  repository = "https://opensearch-project.github.io/helm-charts/"
  chart      = "opensearch-dashboards"
  version    = "1.0.6"
  namespace  = kubernetes_namespace.leafcutter.metadata[0].name
  timeout    = 600

  set {
    name  = "imageTag"
    value = var.opensearch_version
  }

/*
  set {
    name  = "config.opensearch_dashboards\\.yml.server.ssl.enabled"
    value = "false"
  }


  set {
    name  = "config.opensearch_dashboards\\.yml.opensearch.ssl.verificationMode"
    value = "none"
  }

  set {
    name  = "config.opensearch_dashboards\\.yml.opensearch_security.auth.type"
    value = "proxy"
  }

  set {
    name  = "config.opensearch_dashboards\\.yml.opensearch_security.proxycache.user_header"
    value = "x-proxy-user"
  }

  set {
    name  = "config.opensearch_dashboards\\.yml.opensearch_security.proxycache.roles_header"
    value = "x-proxy-roles"
  }

  set {
    name  = "config.opensearch_dashboards\\.yml.opensearch.requestHeadersWhitelist"
    value = yamlencode(["securitytenant","Authorization","x-forwarded-for","x-proxy-user","x-proxy-roles"])
  }
*/
  set {
    name  = "resources.limits.cpu"
    value = "2000m"
  }

  set {
    name  = "resources.limits.memory"
    value = "2000Mi"
  }

  set {
    name  = "resources.requests.cpu"
    value = "2000m"
  }

  set {
    name  = "resources.requests.memory"
    value = "2000Mi"
  }

  set {
    name  = "tolerations[0].key"
    value = "eks.amazonaws.com/compute-type"
  }

  set {
    name  = "tolerations[0].operator"
    value = "Equal"
  }

  set {
    name  = "tolerations[0].value"
    value = "fargate"
  }

  set {
    name  = "tolerations[0].effect"
    value = "NoSchedule"
  }

  set {
    name  = "env[0].name"
    value = "ANOTHER_ONE"
  }

  set {
    name  = "env[0].value"
    value = "efg"
  }
}

resource "helm_release" "leafcutter_web" {
  name = "leafcutter-web"

  repository          = "https://gitlab.com/api/v4/projects/29120429/packages/helm/stable"
  repository_username = var.leafcutter_registry_username
  repository_password = var.leafcutter_registry_password
  chart               = "leafcutter-web"
  version             = var.leafcutter_version
  namespace           = kubernetes_namespace.leafcutter.metadata[0].name
  timeout             = 600

  set {
    name  = "env[0].name"
    value = "GOOGLE_CLIENT_ID"
  }

  set {
    name  = "env[0].value"
    value = var.google_client_id
  }

  set {
    name  = "env[1].name"
    value = "GOOGLE_CLIENT_SECRET"
  }

  set {
    name  = "env[1].value"
    value = var.google_client_secret
  }

  set {
    name  = "env[2].name"
    value = "NEXTAUTH_URL"
  }

  set {
    name  = "env[2].value"
    value = var.nextauth_url
  }

  set {
    name  = "env[3].name"
    value = "OPENSEARCH_DASHBOARDS_URL"
  }

  set {
    name  = "env[3].value"
    value = var.opensearch_dashboards_url
  }

 set {
    name  = "env[4].name"
    value = "NEXTAUTH_SECRET"
  }

  set {
    name  = "env[4].value"
    value = var.nextauth_secret
  }

  set {
    name  = "env[5].name"
    value = "OPENSEARCH_URL"
  }

  set {
    name  = "env[5].value"
    value = var.opensearch_url
  }

  set {
    name  = "env[6].name"
    value = "OPENSEARCH_USERNAME"
  }

  set {
    name  = "env[6].value"
    value = var.opensearch_username
  }

  set {
    name  = "env[7].name"
    value = "OPENSEARCH_PASSWORD"
  }

  set {
    name  = "env[7].value"
    value = var.opensearch_password
  }

  set {
    name  = "env[8].name"
    value = "OPENSEARCH_USER_PASSWORD"
  }

  set {
    name  = "env[8].value"
    value = var.opensearch_user_password
  }

  set {
    name  = "env[9].name"
    value = "APPLE_CLIENT_ID"
  }

  set {
    name  = "env[9].value"
    value = var.apple_client_id
  }

  set {
    name  = "env[10].name"
    value = "APPLE_CLIENT_SECRET"
  }

  set {
    name  = "env[10].value"
    value = var.apple_client_secret
  }

  set {
    name  = "env[11].name"
    value = "NEXTAUTH_URL_INTERNAL"
  }

  set {
    name  = "env[11].value"
    value = var.nextauth_url_internal
  }

  set {
    name  = "imagePullSecrets[0].name"
    value = kubernetes_secret.leafcutter_web_docker_config.metadata[0].name
  }

  set {
    name  = "ingress.enabled"
    value = true
  }

  set {
    name  = "ingress.hosts[0].host"
    value = replace(var.nextauth_url, "https://", "")
  }

  set {
    name  = "ingress.hosts[0].paths[0].path"
    value = "/*"
  }

  set {
    name  = "ingress.hosts[0].paths[0].pathType"
    value = "ImplementationSpecific"
  }

  set {
    name  = "ingress.annotations.kubernetes\\.io/ingress\\.class"
    value = "alb"
  }

  set {
    name  = "ingress.annotations.alb\\.ingress\\.kubernetes\\.io\\/scheme"
    value = "internet-facing"
  }

  set {
    name  = "ingress.annotations.alb\\.ingress\\.kubernetes\\.io\\/target-type"
    value = "ip"
  }

  set {
    name  = "ingress.annotations.alb\\.ingress\\.kubernetes\\.io\\/success-codes"
    value = "200\\,302"
  }

  set {
    name  = "ingress.annotations.alb\\.ingress\\.kubernetes\\.io\\/listen-ports"
    value = "[{\"HTTPS\": 443}]"
  }

  set {
    name  = "ingress.annotations.alb\\.ingress\\.kubernetes\\.io\\/backend-protocol"
    value = "HTTP"
  }

 set {
    name  = "resources.requests.cpu"
    value = "2000m"
  }

  set {
    name  = "resources.requests.memory"
    value = "2000Mi"
  }
}

