
variable "cluster_name" {
  type = string
}

variable "cluster_endpoint" {
  type = string
}

variable "cluster_ca_certificate" {
  type = string
}

variable "leafcutter_registry_username" {
  type = string
}

variable "leafcutter_registry_password" {
  type = string
}

variable "leafcutter_registry_docker_config" {
  type = string
}

variable "leafcutter_version" {
  type = string
}

variable "kms_key_arn" {
  type = string
}

variable "google_client_id" {
  type = string
}

variable "google_client_secret" {
  type = string
}

variable "apple_client_id" {
  type = string
}

variable "apple_client_secret" {
  type = string
}

variable "nextauth_url" {
  type = string
}

variable "nextauth_url_internal" {
  type = string
}

variable "nextauth_secret" {
  type = string
}

variable "opensearch_dashboards_url" {
  type = string
}

variable "opensearch_url" {
  type = string
}

variable "opensearch_username" {
  type = string
}

variable "opensearch_password" {
  type = string
}

variable "opensearch_user_password" {
  type = string
}

variable "opensearch_version" {
  type = string
}
