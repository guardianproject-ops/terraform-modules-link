locals {
  ssm_prefix = module.ssm_prefix.full_prefix

  misp_params_defs = {
    "misp_domain" : { secure : false },
    "misp_email_from_addr" : { secure : false },
    "misp_admin_username" : { secure : true },
    "misp_admin_password" : { secure : true },
    "cloudflare_account_id" : { secure : true },
    "cloudflare_tunnel_name" : { secure : true },
    "cloudflare_tunnel_id" : { secure : true },
    "cloudflare_tunnel_secret" : { secure : true },
  }
  misp_db_params = {
    "misp_db_name" : { secure : false },
    "misp_db_host" : { secure : false },
    "misp_db_username" : { secure : true },
    "misp_db_password" : { secure : true },
  }

  log_groups = {
    "misp" : "zammad_railsserver",
    "cloudflared" : "cloudflared",
    "journald" : "journald",
  }

  email_data = jsondecode(data.aws_ssm_parameter.email.value)
  misp_params = merge(var.misp_params, {
    "misp_email_hostname" : local.email_data.smtp_server,
    "misp_email_port" : local.email_data.smtp_port,
    "misp_email_username" : local.email_data.smtp_username,
    "misp_email_password" : local.email_data.smtp_password
  })
}

data "aws_region" "current" {}

data "aws_ssm_parameter" "email" {
  name = var.ssm_email_path

  provider = aws.operations
}

module "ssm_prefix" {
  source            = "git::https://gitlab.com/guardianproject-ops/terraform-aws-ssm-param-store-iam?ref=master"
  path_prefix       = "/${module.this.id}"
  prefix_with_label = false
  region            = data.aws_region.current.id
  context           = module.this.context
  kms_key_arn       = var.kms_key_arn
}

resource "aws_ssm_parameter" "param" {
  for_each = local.misp_params_defs
  name     = "${local.ssm_prefix}/${each.key}"
  type     = each.value.secure ? "SecureString" : "String"
  value    = var.misp_params[each.key]
  tags     = module.this.tags
}

resource "aws_ssm_parameter" "db_param" {
  for_each = local.misp_db_params
  name     = "${local.ssm_prefix}/${each.key}"
  type     = each.value.secure ? "SecureString" : "String"
  value    = var.misp_db_params[each.key]
  tags     = module.this.tags
}

resource "aws_cloudwatch_log_group" "app" {
  for_each          = local.log_groups
  name              = "/${module.this.id}-${each.key}"
  retention_in_days = var.log_retention_days
}

resource "aws_ssm_parameter" "log_group_app" {
  for_each  = local.log_groups
  name      = "${local.ssm_prefix}/log_group_${each.value}"
  value     = aws_cloudwatch_log_group.app[each.key].name
  type      = "String"
  overwrite = true
}
