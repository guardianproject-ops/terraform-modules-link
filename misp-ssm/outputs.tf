output "ssm_prefix" {
  value = local.ssm_prefix
}
output "ssm_prefix_policy_arn" {
  value = module.ssm_prefix.policy_arn
}
output "email_data" {
  value = local.email_data
}
