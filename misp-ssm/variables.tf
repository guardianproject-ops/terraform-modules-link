variable "log_retention_days" {
  type        = number
  description = "the number of days to retain logs in cloudwatch"
  default     = 14
}

variable "log_group_journald" {
  type        = string
  description = "the log group name for journald logs"
}

variable "aws_region" {
  type = string
}

variable "kms_key_arn" {
  type = string
}

variable "misp_params" {
  type = map(string)
}

variable "misp_db_params" {
  type = map(string)
}

variable "ssm_email_path" {
  type        = string
  description = "The SSM Parameter Store path to the SMTP credentials json"
}
