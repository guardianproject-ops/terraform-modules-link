module "label" {
  source  = "cloudposse/label/null"
  version = "0.23.0"
  context = module.this.context
  name    = "misp"
  tags    = merge(module.this.tags, { Application = "misp" })
}

data "aws_region" "current" {}

data "aws_caller_identity" "current" {}

module "instance" {
  source                    = "../ec2-instance"
  is_prod_like              = var.is_prod_like
  ssm_prefix                = var.ssm_prefix
  ssm_prefix_policy_arn     = var.ssm_prefix_policy_arn
  kms_key_arn               = var.kms_key_arn
  secretsmanager_ssh_key_id = var.secretsmanager_ssh_key_id
  vpc_id                    = var.vpc_id
  vpc_cidr_block            = var.vpc_cidr_block
  availability_zone_1       = var.availability_zone_1
  subnet_id                 = var.subnet_id
  disk_allocation_gb        = var.disk_allocation_gb
  instance_type             = var.instance_type
  ami_filter = {
    "name" : "gold-debian-base-${var.ami_account_name}-prod-buster-*"
  }
  ami_owners               = [var.ami_account_id]
  secure_metadata_endpoint = var.secure_metadata_endpoint
  context                  = module.label.context
}
