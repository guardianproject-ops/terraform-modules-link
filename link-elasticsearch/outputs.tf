output "elasticsearch_endpoint" {
  value       = module.elasticsearch.domain_endpoint
  description = "Domain-specific endpoint used to submit index, search, and data upload requests"
}

output "elasticsearch_domain_id" {
  value       = module.elasticsearch.domain_id
  description = "Unique identifier for the Elasticsearch domain"
}

output "elasticsearch_arn" {
  value       = module.elasticsearch.domain_arn
  description = "ARN of the Elasticsearch domain"
}

output "elasticsearch_security_group_id" {
  value       = module.elasticsearch.security_group_id
  description = "Security Group ID to control access to the Elasticsearch domain"
}

output "elasticsearch_inbound_security_group_id" {
  value       = aws_security_group.inbound.id
  description = "Security Group ID that is allowed to access the Elasticsearch domain (Assign this to your instances)"
}
