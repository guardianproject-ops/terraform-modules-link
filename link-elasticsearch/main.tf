module "label" {
  source  = "git::https://github.com/cloudposse/terraform-null-label.git?ref=tags/0.19.2"
  context = module.this.context
  tags    = merge(module.this.tags, { Application = "elasticsearch" })
}

module "label_sg" {
  source     = "git::https://github.com/cloudposse/terraform-null-label.git?ref=tags/0.19.2"
  context    = module.this.context
  attributes = ["inbound-sg"]
}

resource "aws_security_group" "inbound" {
  # this security group acts as a marker
  # instances in this group are allowed to
  # talk to the elastic search domain
  name   = module.label_sg.id
  vpc_id = var.vpc_id
  tags   = module.label_sg.tags
}


module "elasticsearch" {
  source = "git::https://github.com/cloudposse/terraform-aws-elasticsearch.git?ref=tags/0.26.0"

  vpc_id                     = var.vpc_id
  subnet_ids                 = [var.subnet_id]
  elasticsearch_version      = "7.9"
  instance_type              = var.instance_type
  instance_count             = 1
  availability_zone_count    = 1
  zone_awareness_enabled     = false
  warm_enabled               = false
  dedicated_master_enabled   = false
  ebs_volume_size            = var.allocated_disk_gb
  kibana_subdomain_name      = "kibana-es"
  encrypt_at_rest_enabled    = "true"
  security_groups            = [aws_security_group.inbound.id]
  encrypt_at_rest_kms_key_id = var.kms_key_id
  advanced_options = {
    "rest.action.multi.allow_explicit_index" = "true"
  }

  context = module.label.context
}


resource "aws_elasticsearch_domain_policy" "default" {
  domain_name     = module.elasticsearch.domain_name
  access_policies = <<POLICIES
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "AWS": [
          "*"
        ]
      },
      "Action": [
        "es:*"
      ],
      "Resource": "${module.elasticsearch.domain_arn}/*"
    }
  ]
}
POLICIES
}
