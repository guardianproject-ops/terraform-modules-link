output "db_address" {
  description = "The address/hostname of the RDS instance"
  value       = module.misp_rds.this_db_instance_address
}

output "db_arn" {
  description = "The ARN of the RDS instance"
  value       = module.misp_rds.this_db_instance_arn
}

output "db_id" {
  description = "The id of the RDS instance"
  value       = module.misp_rds.this_db_instance_id
}

output "db_endpoint" {
  description = "The connection endpoint"
  value       = module.misp_rds.this_db_instance_endpoint
}

output "db_name" {
  description = "The database name"
  value       = module.misp_rds.this_db_instance_name
}

output "db_username" {
  description = "The master username for the database"
  value       = module.misp_rds.this_db_instance_username
}

output "db_password" {
  description = "The database password (this password may be old, because Terraform doesn't track it after initial creation)"
  value       = module.misp_rds.this_db_instance_password
  sensitive   = true
}

output "db_port" {
  description = "The database port"
  value       = module.misp_rds.this_db_instance_port
}
