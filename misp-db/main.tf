resource "aws_security_group" "rds" {
  vpc_id = var.vpc_id

  ingress {
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = [var.vpc_cidr_block]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = module.this.tags
}

module "misp_rds" {
  source     = "terraform-aws-modules/rds/aws"
  version    = "~> v2.20.0"
  identifier = module.this.id

  engine               = "mysql"
  instance_class       = var.instance_class
  major_engine_version = "8.0"
  engine_version       = "8.0.20"
  family               = "mysql8.0"
  allocated_storage    = var.allocated_storage
  port                 = "3306"

  storage_encrypted = true
  kms_key_id        = var.kms_key_arn

  name     = var.admin_database_name
  username = var.admin_database_username
  password = var.admin_database_password

  subnet_ids              = var.subnet_ids
  vpc_security_group_ids  = [aws_security_group.rds.id]
  multi_az                = false
  maintenance_window      = "Mon:00:00-Mon:03:00"
  backup_window           = "03:00-06:00"
  backup_retention_period = var.backup_retention_period
  create_db_option_group  = false

  create_monitoring_role = true
  monitoring_interval    = "60"
  monitoring_role_name   = "AllowRDSMonitoringFor-${module.this.id}"


  final_snapshot_identifier   = "${module.this.id}-final-snapshot"
  deletion_protection         = var.is_prod_like
  apply_immediately           = true
  skip_final_snapshot         = !var.is_prod_like
  allow_major_version_upgrade = false

  tags = module.this.tags
}
