variable "is_prod_like" {
  type        = bool
  description = "Flag to indicate this module is prod like or not. Some behavior will be different in prod like envs"
}

variable "kms_key_arn" {
  type        = string
  description = "the kms key to use to encrypt the database"
}

variable "vpc_cidr_block" {
  type = string
}

variable "vpc_id" {
  type = string
}

variable "admin_database_name" {
  type = string
}

variable "admin_database_username" {
  type = string
}

variable "admin_database_password" {
  type = string
}

variable "instance_class" {
  type = string
}

variable "allocated_storage" {
  type = number
}


variable "subnet_ids" {
  type = list(string)
}

variable "backup_retention_period" {
  type    = number
  default = 30
}
